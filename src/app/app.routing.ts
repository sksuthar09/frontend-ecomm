import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { LoginComponent } from './login/login.component';
import { ShopComponent } from './shop/shop.component';
import { MenComponent } from './shop/men/men.component';
import { WomenComponent } from './shop/women/women.component';
import { ErrorpageComponent } from './errorpage/errorpage.component';
import { PaComponent } from './pa/pa.component';
import { FiltersComponent } from './pa/filters/filters.component';
import { ProductListsComponent } from './pa/product-lists/product-lists.component';
import { ProductDetailsComponent } from './pa/product-details/product-details.component';
import { CartComponent } from './cart/cart.component';
import { CheckoutComponent } from './checkout/checkout.component';
import { AuthGuard } from './_helpers/auth.guard';

const routes: Routes = [
    // { path: '', component: HomeComponent, canActivate: [AuthGuard] },
   {  path: '', component: HomeComponent },
   {  path: 'login', component: LoginComponent },
   {  path: 'details', component: ProductDetailsComponent },
   {  path: 'cart', component: CartComponent },
   {  path: 'checkout', component: CheckoutComponent },
   {  path: 'pa', component: PaComponent,
      children : [
         { path: '', component:ProductDetailsComponent }
      ]
   },
   {  path: 'shop', component: ShopComponent,
      children: [
         { path: 'men', component: MenComponent },
         { path: 'women', component: WomenComponent}
      ]
   },

   // 404 redirect to home
   // { path: '**', redirectTo: '' }
   { path: '**', component: ErrorpageComponent }
];

export const appRoutingModule = RouterModule.forRoot(routes);