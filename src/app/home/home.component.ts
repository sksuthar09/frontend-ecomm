import { Component } from '@angular/core';
import { first } from 'rxjs/operators';

import { User } from '../_models/user';
import { AuthenticationService } from '../_services/authentication.service';
import { UserService } from '../_services/user.service';

@Component({ templateUrl: 'home.component.html' })
export class HomeComponent {

   loading = false;
   users: User[];
   currentUser: User;
    // constructor(private userService: UserService) { }
    constructor() {
      this.currentUser = JSON.parse(localStorage.getItem('currentUser'));
   }

   carouselOptions = {
      margin: 10,
      items: 2,
      nav: true,
      dots: false,
      // autoPlay : true,
        autoplay:true,
      navText: ["<div class='nav-btn prev-slide pre-f'><img src='/assets/images/previous.png'/></div>", "<div class='nav-btn next-slide'><img src='/assets/images/next.png'/></div>"],
      responsiveClass: true,
    // responsive: {
    //   0: {
    //     items: 1,
    //     nav: true
    //   },
    //   600: {
    //     items: 1,
    //     nav: true
    //   },
    //   1000: {
    //     items: 2,
    //     nav: true,
    //     loop: false
    //   },
    //   1500: {
    //     items: 3,
    //     nav: true,
    //     loop: false
    //   }
    // }
   }

   images = [
      {
         text: "Everfresh Flowers",
         image: "assets/images/slider/slider1.jpg"
      },
      {
         text: "Festive Deer",
         image: "assets/images/slider/slider2.jpg"
      },
      {
         text: "Morning Greens",
         image: "assets/images/slider/slider1.jpg"
      },
      {
         text: "Bunch of Love",
         image: "assets/images/slider/slider2.jpg"
      },
      {
         text: "Blue Clear",
         image: "assets/images/slider/slider1.jpg"
      }
   ]

   carouselOptionsWhatsNew = {
      margin: 10,
      items: 5,
      nav: true,
      dots: false,
      navText: ["<div class='nav-btn prev-slide pre-f'><img src='/assets/images/previous.png'/></div>", "<div class='nav-btn next-slide'><img src='/assets/images/next.png'/></div>"],
      responsiveClass: true,
   }

   ImagesWhatsNew = [
      {
         text: "Everfresh Flowers",
         image: "assets/images/deals/img1.jpg"
      },
      {
         text: "Festive Deer",
         image: "assets/images/deals/img2.jpg"
      },
      {
         text: "Morning Greens",
         image: "assets/images/deals/img1.jpg"
      },
      {
         text: "Bunch of Love",
         image: "assets/images/deals/img2.jpg"
      },
      {
         text: "Blue Clear",
         image: "assets/images/deals/img1.jpg"
      },
       {
         text: "Blue Clear",
         image: "assets/images/deals/img2.jpg"
      },
       {
         text: "Blue Clear",
         image: "assets/images/deals/img1.jpg"
      }
   ]


   ngOnInit() {
      this.loading = false;
        // this.userService.getAll().pipe(first()).subscribe(users => {
        //     this.loading = false;
        //     this.users = users;
        // });
      }
   }