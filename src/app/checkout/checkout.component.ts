import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-checkout',
  templateUrl: './checkout.component.html',
  styleUrls: ['./checkout.component.css']
})
export class CheckoutComponent implements OnInit {

   section1: Boolean = false;
   section2: Boolean = true;
   section3: Boolean = false;
   section4: Boolean = false;

   constructor() { }
       result = {
    "addresses": [
        {
            "id": 1,
            "address": "virar",
            "alternate_mobile": "9999999999",
            "city": "Mumbai",
            "country": "India",
            "first_name": "Suresh",
            "landmark": "Yazoo park",
            "last_name": "kumar",
            "mobile": "9689326387",
            "name": "suresh",
            "pincode": "444444",
            "state": "Maharashtra",
            "status": "enabled",
            "sub_type": "home",
            "user_id": 2
        },
         {
            "id": 2,
            "address": "virar",
            "alternate_mobile": "9999999999",
            "city": "Mumbai",
            "country": "India",
            "first_name": "Suresh",
            "landmark": "Yazoo park",
            "last_name": "kumar",
            "mobile": "9876453456",
            "name": "suresh",
            "pincode": "444444",
            "state": "Maharashtra",
            "status": "enabled",
            "sub_type": "home",
            "user_id": 2
        },
         {
            "id": 3,
            "address": "virar",
            "alternate_mobile": "9999999999",
            "city": "Mumbai",
            "country": "India",
            "first_name": "Suresh",
            "landmark": "Yazoo park",
            "last_name": "kumar",
            "mobile": "9087656543",
            "name": "suresh",
            "pincode": "444444",
            "state": "Maharashtra",
            "status": "enabled",
            "sub_type": "home",
            "user_id": 2
        },
    ],
     "active_address": [
        {
            "id": 1,
            "address": "virar",
            "alternate_mobile": "9999999999",
            "city": "Mumbai",
            "country": "India",
            "first_name": "Suresh",
            "landmark": "Yazoo park",
            "last_name": "kumar",
            "mobile": "88888888",
            "name": "suresh",
            "pincode": "444444",
            "state": "Maharashtra",
            "status": "enabled",
            "sub_type": "home",
            "user_id": 2
        }
    ]
};
   ngOnInit() { 
      var  result = {
    "addresses": [
        {
            "id": 1,
            "address": "virar",
            "alternate_mobile": "9999999999",
            "city": "Mumbai",
            "country": "India",
            "first_name": "Suresh",
            "landmark": "Yazoo park",
            "last_name": "kumar",
            "mobile": "88888888",
            "name": "suresh",
            "pincode": "444444",
            "state": "Maharashtra",
            "status": "enabled",
            "sub_type": "home",
            "user_id": 2
        },
         {
            "id": 2,
            "address": "virar",
            "alternate_mobile": "9999999999",
            "city": "Mumbai",
            "country": "India",
            "first_name": "Suresh",
            "landmark": "Yazoo park",
            "last_name": "kumar",
            "mobile": "88888888",
            "name": "suresh",
            "pincode": "444444",
            "state": "Maharashtra",
            "status": "enabled",
            "sub_type": "home",
            "user_id": 2
        },
         {
            "id": 3,
            "address": "virar",
            "alternate_mobile": "9999999999",
            "city": "Mumbai",
            "country": "India",
            "first_name": "Suresh",
            "landmark": "Yazoo park",
            "last_name": "kumar",
            "mobile": "88888888",
            "name": "suresh",
            "pincode": "444444",
            "state": "Maharashtra",
            "status": "enabled",
            "sub_type": "home",
            "user_id": 2
        },
    ],
     "active_address": [
        {
            "id": 1,
            "address": "virar",
            "alternate_mobile": "9999999999",
            "city": "Mumbai",
            "country": "India",
            "first_name": "Suresh",
            "landmark": "Yazoo park",
            "last_name": "kumar",
            "mobile": "88888888",
            "name": "suresh",
            "pincode": "444444",
            "state": "Maharashtra",
            "status": "enabled",
            "sub_type": "home",
            "user_id": 2
        }
    ]
};
      console.log(result);
   }

   showLoginSection(){
      this.section1 =true;
      this.section2 =false;
      this.section3 =false;
      this.section4 =false;
   }

   showDeliveryAddress(){
     this.section1 =false;
     this.section2 =true;
     this.section3 =false;
     this.section4 =false;
   }

   showOrderSummary(){
     this.section1 =false;
     this.section2 =false;
     this.section3 =true;
     this.section4 =false;
   }

   showPaymentDetails(){
     this.section1 =false;
     this.section2 =false;
     this.section3 =false;
     this.section4 =true;
   }
}
