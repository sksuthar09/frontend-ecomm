import { Component, OnInit } from '@angular/core';
import { User } from '../_models/user';
import { AuthenticationService } from '../_services/authentication.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

   currentUser: User;

   constructor(private authenticationService: AuthenticationService) { }

   
  
   ngOnInit() {
      this.currentUser = JSON.parse(localStorage.getItem('currentUser'));
   }


   isLoggedIn(){
      if(this.currentUser!=null){
         console.log(this.currentUser);
         return true;
      }
      else
         return false;
   }

   logout(){
      this.authenticationService.logout();
      this.currentUser = null;
   }
}
