import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-product-details',
  templateUrl: './product-details.component.html',
  styleUrls: ['./product-details.component.css']
})
export class ProductDetailsComponent implements OnInit {

    img = 'assets/images/product/product1.jpg';
   img2 = 'assets/images/product/product-d-1.jpg';
  constructor() { }

  ngOnInit() {
   this.loadScript('assets/js/vendor.js');
   this.loadScript('assets/js/jquery.shopnav.js');
   this.loadScript('assets/js/app.js');
  }
   public loadScript(url: string) {
    const body = <HTMLDivElement> document.body;
    const script = document.createElement('script');
    script.innerHTML = '';
    script.src = url;
    script.async = false;
    script.defer = true;
    body.appendChild(script);
  }

}
