import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';

// used to create fake backend
import { fakeBackendProvider } from './_helpers/fake-backend';
import { AppComponent } from './app.component';
import { appRoutingModule } from './app.routing';
import { NgxImageZoomModule } from 'ngx-image-zoom';
import { JwtInterceptor } from './_helpers/jwt.interceptor';
import { ErrorInterceptor } from './_helpers/error.interceptor';
import { HomeComponent } from './home/home.component';
import { LoginComponent } from './login/login.component';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { OwlModule } from 'ngx-owl-carousel';
import { AccessoriesComponent } from './accessories/accessories.component';
import { ShopComponent } from './shop/shop.component';
import { OfferZoneComponent } from './shop/offer-zone/offer-zone.component';
import { WomenComponent } from './shop/women/women.component';
import { MenComponent } from './shop/men/men.component';
import { PaComponent } from './pa/pa.component';
import { ErrorpageComponent } from './errorpage/errorpage.component';
import { FiltersComponent } from './pa/filters/filters.component';
import { ProductListsComponent } from './pa/product-lists/product-lists.component';
import { ProductDetailsComponent } from './pa/product-details/product-details.component';
import { CartComponent } from './cart/cart.component';
import { CheckoutComponent } from './checkout/checkout.component';
import { AccordionComponent } from './checkout/accordion/accordion.component';
import { ToggleComponent } from './checkout/toggle/toggle.component';
@NgModule({
    imports: [
        BrowserModule,
        ReactiveFormsModule,
        HttpClientModule,
        appRoutingModule,
        OwlModule,
        NgxImageZoomModule 
    ],
    declarations: [
        AppComponent,
        HomeComponent,
        LoginComponent,
        HeaderComponent,
        FooterComponent,
        AccessoriesComponent,
        ShopComponent,
        OfferZoneComponent,
        WomenComponent,
        MenComponent,
        PaComponent,
        ErrorpageComponent,
        FiltersComponent,
        ProductListsComponent,
        ProductDetailsComponent,
        CartComponent,
        CheckoutComponent,
        AccordionComponent,
        ToggleComponent
    ],
    providers: [
        { provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true },
        { provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptor, multi: true },

        // provider used to create fake backend
        fakeBackendProvider
    ],
    bootstrap: [AppComponent]
})
export class AppModule { }